import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { useAuth } from '../context/AuthContext';
import ReactPlayer from "react-player";
import like from '../assets/like.png'
import Joker from '../assets/joker.mp4';

const Peliculas = () => {
  const [peliculas, setPeliculas] = useState([]);
  const auth = useAuth();
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  // eslint-disable-next-line
  const [messageTimeout, setMessageTimeout] = useState(null);

  const getPeliculas = useCallback(async () => {
    try {
      const response = await axios.get(
        'http://localhost:5000/film-app-f61ea/us-central1/app/api/peliculas',
        {}
      );

      setPeliculas(response.data);
    } catch (error) {
      console.error('Error al obtener las películas:', error);
    }
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      await getPeliculas();
    };

    fetchData();
  }, [getPeliculas]);

  const handleCorazonClick = async (peliculaId) => {
    setErrorMessage('');
    setMessage('');
    console.log(`Se hizo clic en el corazón de la película con ID: ${peliculaId}`);
  
    try {
      const response = await axios.post('http://localhost:5000/film-app-f61ea/us-central1/app/api/favoritos', {
        uid: auth.user.uid,
        peliculaID: peliculaId,
      });
  
      if (response.status === 200) {
        console.log('Película agregada a favoritos exitosamente');
        setMessage('Película agregada a favoritos exitosamente');
        const timeout = setTimeout(() => {
          setMessage('');
        }, 3000);

        setMessageTimeout(timeout);
      }
    } catch (error) {
      if (error.response && error.response.status === 400) {
        console.warn('La película ya está en la lista de favoritos');
        setErrorMessage('La película ya está en la lista de favoritos');
        const timeout = setTimeout(() => {
          setErrorMessage('');
        }, 3000);

        setMessageTimeout(timeout);

      } else {
        console.error('Error al agregar la película a favoritos:', error);
      }
    }
  };
  
  
  return (
    <>
      <div className="peliculas-dos">
        <ReactPlayer className="video"
          width='100%'
          height='auto'
          url={Joker}
          playing
          controls
        />
      </div>
      <div className="search-bar">
        <input type="text" id='buscar' placeholder="Buscar películas..." />
        <button>Buscar</button>
      </div>
      <section>
      {message && <div className='alert alert-success'>{message}</div>}
      {errorMessage && <div className='alert alert-danger'>{errorMessage}</div>}
        <div className="movie-list">
          {peliculas.map((pelicula) => (
            <div className="movie-card" key={pelicula.id}>
              <div className="movie-title">{pelicula.titulo}</div>
              <div className="movie-image">
                <img src={pelicula.imagenURL} alt={`Póster de ${pelicula.titulo}`} />
              </div>
              <div className="descripcion-container">
                <div className="movie-description">{pelicula.descripcion}</div>
                {auth.user ? (
                <div className="favorito">
                  <button onClick={() => handleCorazonClick(pelicula.id)}>
                    <img src={like} alt="Corazón" className="corazon" />
                  </button>
                </div>
                ) : null}
              </div>
            </div>
          ))}
        </div>
      </section>
    </>
  );
};

export default Peliculas;
