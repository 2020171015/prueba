import React from 'react';
import './SplashScreen.css';
import splash from './assets/splash.gif'

const SplashScreen = () => {
  return (
    <div className="splash-screen">
      <h1>FILM</h1>
      <p>Cargando...</p>
      <img src={splash} alt="Logo" />
    </div>
  );
};

export default SplashScreen;
