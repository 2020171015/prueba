import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Navbar } from '../components/Navbar';

export const Register = () => {
  const auth = useAuth();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleRegister = async (e) => {
    e.preventDefault();
    setErrorMessage('');
    setMessage('');
  
    try {
      const successMessage = await auth.register(email, password, name);
      setMessage(successMessage);
    } catch (error) {
      setErrorMessage(error.message);
    }
  };
  
  
  
  return (
    <div>
      <Navbar/>
      <div className='container'>
        <br />
        <h2>REGISTRARSE</h2>
        <hr />
        {message && <div className='alert alert-success'>{message}</div>}
        {errorMessage && <div className='alert alert-danger'>{errorMessage}</div>}
        <form autoComplete='off' className='form-group' onSubmit={handleRegister}>
          <div className="input-container">
            <label htmlFor='name'>Nombre</label>
            <input
              type='text'
              className='form-control'
              id='name'
              required
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <br/>
          <div className="input-container">
            <label htmlFor='email'>Correo</label>
            <input
              type='email'
              className='form-control'
              id='email'
              required
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            />
          </div>
          <br />
          <div className="input-container">
            <label htmlFor='password'>Contraseña</label>
            <input
              type='password'
              className='form-control'
              id='password'
              required
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
          </div>
          <br />
          <div className="button-container">
            <button type='submit' className='btn btn-success btn-md mybtn'>
              Registrarme
            </button>
          </div>
        </form>
        <br />
        <br />
        <span>
          ¿Ya tienes una cuenta? Inicia Sesión
          <Link to='/login'>Aquí</Link>
        </span>
      </div>
    </div>
  );
};
