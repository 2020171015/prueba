import React, { useEffect, useState } from 'react';
import { useAuth } from '../context/AuthContext';
import axios from 'axios';
import { Navbar } from '../components/Navbar';

export const Profile = () => {
  const auth = useAuth();
  const [newName, setNewName] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [userInfo, setUserInfo] = useState(null);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');


  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        if (auth.user && auth.user.uid) {
          const uid = auth.user.uid;
          const response = await axios.get(`http://localhost:5000/film-app-f61ea/us-central1/app/api/users/${uid}`);
  
          if (response.data) {
            setUserInfo(response.data);
            setNewName(response.data.name || '');
          } else {
            console.error('No se encontró la información del usuario en la base de datos.');
          }
        } else {
          console.error('El usuario no está autenticado.');
        }
      } catch (error) {
        console.error('Error al obtener la información del usuario:', error);
      } finally {
        setLoading(false);
      }
    };
  
    if (auth.user) {
      fetchUserInfo();
    }
  }, [auth.user]);
  

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        await auth.checkAuthentication();
      } catch (error) {
        console.error('Error al verificar la autenticación:', error);
      }
    };

    checkAuthentication();
  }, [auth]);

  const handleUpdateProfile = async (e) => {
    e.preventDefault();
    setErrorMessage('');
    setMessage('');
  
    try {
      if (userInfo) {
        const userId = userInfo.id;
  
        await axios.put(`http://localhost:5000/film-app-f61ea/us-central1/app/api/users/${userId}`, {
          name: newName,
          password: newPassword,
        });
  
        console.log('Perfil actualizado exitosamente');
        setMessage('Perfil actualizado exitosamente');
      } else {
        console.error('No se puede actualizar el perfil porque no se ha obtenido la información del usuario.');
      }
    } catch (error) {
      console.error('Error al actualizar el perfil:', error);
      setErrorMessage('Error al editar, verifica que la contraseña tenga minimo 6 digitos');
    }
  };
  

  if (loading) {
    return <p>Cargando...</p>;
  }

  return (
    <div>
      <Navbar />
      <div className='container'>
        <br />
        <h2>MI PERFIL</h2>
        <hr />
        {message && <div className='alert alert-success'>{message}</div>}
        {errorMessage && <div className='alert alert-danger'>{errorMessage}</div>}
        {auth.user ? (
          <form autoComplete='off' className='form-group'>
            <div className="input-container">
              <label htmlFor='newEmail'>Correo Electrónico</label>
              <div className="styled-input">{auth.user.email}</div>
            </div>
            <br/>
            <div className="input-container">
              <label htmlFor='newName'>Nuevo Nombre</label>
              <input
                type='text'
                className='form-control'
                id='newName'
                onChange={(e) => setNewName(e.target.value)}
                value={newName}
                autoComplete='new-name'
              />
            </div>
            <br />
            <div className="input-container">
            <label htmlFor='newPassword'>Nueva Contraseña</label>
            <input
              type='password'
              className='form-control'
              id='newPassword'
              onChange={(e) => setNewPassword(e.target.value)}
              value={newPassword}
              autoComplete='new-password'
            />
            </div>
            <br />
            <div className="button-container">
              <button type='submit' className='btn btn-primary btn-md mybtn' onClick={handleUpdateProfile}>
                Actualizar Perfil
              </button>
            </div>
          </form>
        ) : (
          <p>No estás autenticado.</p>
        )}
        <br />
      </div>
    </div>
  );
};
