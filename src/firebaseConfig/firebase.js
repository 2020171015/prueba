import { initializeApp } from 'firebase/app';
import {getFirestore} from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAEzUz_AR8rYAVmQaTSeHWRUxpNffyuvCw",
  authDomain: "film-app-f61ea.firebaseapp.com",
  projectId: "film-app-f61ea",
  storageBucket: "film-app-f61ea.appspot.com",
  messagingSenderId: "1035227569978",
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth(app);