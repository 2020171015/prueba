const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());

app.use('/sw.js', express.static('public/sw'));

admin.initializeApp({
  credential: admin.credential.cert('./permissions.json'),
  databaseURL: "https://film-app-f61ea-default-rtdb.firebaseio.com",
});

const db = admin.firestore();

app.get('/hello-world', (req, res) => {
    return res.status(200).json({message:'hola mundo'});
});

app.post('/api/users', async (req, res) => {
    try {
        await db
            .collection('users')
            .doc()
            .create({
                name: req.body.name,
                favoritos: [],
            });
            return res.status(200).json();
    } catch (error) {
        return res.status(500).send(error);
    }
});

app.get('/api/peliculas', async (req, res) => {
        try {
            const query = db.collection('peliculas')
;            const querySnapShot = await query.get();
            const docs = querySnapShot.docs;
            const response = docs.map((doc) => ({
                id: doc.id,
                titulo: doc.data().titulo,
                imagenURL: doc.data().imagenURL,
                descripcion: doc.data().descripcion,
            }));
            return res.status(200).json(response);
        } catch (error) {
            return res.status(500).json();
        }
});

app.get('/api/users', async (req, res) => {
    try {
        const usersRef = db.collection('users');
        const usersSnapshot = await usersRef.get();
        
        const users = [];
        usersSnapshot.forEach((doc) => {
            users.push({
                id: doc.id,
                name: doc.data().name,
            });
        });

        return res.status(200).json(users);
    } catch (error) {
        return res.status(500).json({ error: error.message });
    }
});

app.get('/api/users/:uid', async (req, res) => {
    try {
        console.log('Received GET request for:', req.url);

        const uid = req.params.uid;
        console.log('UID from URL:', uid);

        const usersRef = db.collection('users');
        const userSnapshot = await usersRef.where('uid', '==', uid).get();

        if (userSnapshot.empty) {
            console.log('User not found');
            return res.status(404).json({ error: 'Usuario no encontrado' });
        }

        const userDoc = userSnapshot.docs[0];
        const userData = {
            id: userDoc.id,
            name: userDoc.data().name,
            email: userDoc.data().email,
        };

        console.log('User data:', userData);

        return res.status(200).json(userData);
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ error: 'Error al obtener el usuario' });
    }
});

app.put('/api/users/:userId', async (req, res) => {
    try {
        const userId = req.params.userId;
        const updatedData = req.body;

        const userRef = db.collection('users').doc(userId);

        const userSnapshot = await userRef.get();
        const userFirestoreData = userSnapshot.data();
        const authUid = userFirestoreData.uid;

        await userRef.update({
            name: updatedData.name,
        });

        await admin.auth().updateUser(authUid, {
            password: updatedData.password || undefined,
        });

        return res.status(200).json({ message: 'Usuario actualizado exitosamente' });
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ error: 'Error al actualizar el usuario' });
    }
});


// Agregar a la colección de favoritos
app.post('/api/favoritos', async (req, res) => {
    try {
      const userUID = req.body.uid;
      const peliculaID = req.body.peliculaID;
  
      const userRef = db.collection('users').where('uid', '==', userUID);
      const userSnapshot = await userRef.get();
  
      if (userSnapshot.empty) {
        return res.status(404).json({ error: 'Usuario no encontrado' });
      }
  
      const userID = userSnapshot.docs[0].id;
  
      const favoritosRef = db.collection('favoritos').doc(userID);
      const favoritosSnapshot = await favoritosRef.get();
  
      let favoritosArray = [];
  
      if (favoritosSnapshot.exists) {
        favoritosArray = favoritosSnapshot.data().favoritos || [];
  
  
        if (favoritosArray.includes(peliculaID)) {
          return res.status(400).json({ error: 'La película ya está en la lista de favoritos' });
        }
      }
  
      const peliculaRef = await db.collection('peliculas').doc(peliculaID).get();
  
      if (!peliculaRef.exists) {
        return res.status(404).json({ error: 'Película no encontrada' });
      }
  
      favoritosArray.push(peliculaID);
  
      await favoritosRef.set({ favoritos: favoritosArray,  userID, userUID });
  
      return res.status(200).json({ message: 'Película agregada a favoritos exitosamente' });
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ error: error.message });
    }
  });
  
  app.get('/api/favoritos/:uid', async (req, res) => {
    try {
      const uid = req.params.uid;
  
      // Buscar el usuario por su UID
      const userRef = db.collection('users').where('uid', '==', uid);
      const userSnapshot = await userRef.get();
  
      if (userSnapshot.empty) {
        return res.status(404).json({ error: 'Usuario no encontrado' });
      }
  
      const userID = userSnapshot.docs[0].id;
  
      // Obtener las películas favoritas del usuario
      const favoritosRef = db.collection('favoritos').doc(userID);
      const favoritosSnapshot = await favoritosRef.get();
  
      if (!favoritosSnapshot.exists) {
        return res.status(200).json({ message: 'El usuario no tiene películas en la lista de favoritos' });
      }
  
      const favoritosArray = favoritosSnapshot.data().favoritos || [];
  
      // Obtener información de las películas favoritas y sus comentarios
      const peliculasFavoritas = await Promise.all(
        favoritosArray.map(async (peliculaID) => {
          // Obtener información de la película
          const peliculaRef = await db.collection('peliculas').doc(peliculaID).get();
  
          if (peliculaRef.exists) {
            // Obtener comentarios asociados a la película y al usuario
            const comentarioRef = db.collection('comentarios')
              .where('peliculaID', '==', peliculaID)
              .where('userID', '==', userID);
  
            const comentarioSnapshot = await comentarioRef.get();
            const comentarios = comentarioSnapshot.docs.map(doc => doc.data().comentario);
  
            return {
              id: peliculaRef.id,
              titulo: peliculaRef.data().titulo,
              imagenURL: peliculaRef.data().imagenURL,
              descripcion: peliculaRef.data().descripcion,
              comentarios: comentarios,
            };
          } else {
            return null;
          }
        })
      );
  
      // Filtrar películas nulas y responder con la información
      return res.status(200).json(peliculasFavoritas.filter((pelicula) => pelicula !== null));
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ error: error.message });
    }
  });
  
  
  app.delete('/api/favoritos', async (req, res) => {
    try {
      const userUID = req.body.uid;
      const peliculaID = req.body.peliculaID;
  
      const userRef = db.collection('users').where('uid', '==', userUID);
      const userSnapshot = await userRef.get();
  
      if (userSnapshot.empty) {
        return res.status(404).json({ error: 'Usuario no encontrado' });
      }
  
      const userID = userSnapshot.docs[0].id;
  
      const favoritosRef = db.collection('favoritos').doc(userID);
      const favoritosSnapshot = await favoritosRef.get();
  
      let favoritosArray = [];
  
      if (favoritosSnapshot.exists) {
        favoritosArray = favoritosSnapshot.data().favoritos || [];
  
        if (favoritosArray.includes(peliculaID)) {
          favoritosArray = favoritosArray.filter(id => id !== peliculaID);
  
          await favoritosRef.set({ favoritos: favoritosArray, userID, userUID });
  
          return res.status(200).json({ message: 'Película eliminada de favoritos exitosamente' });
        } else {
          return res.status(400).json({ error: 'La película no está en la lista de favoritos' });
        }
      } else {
        return res.status(400).json({ error: 'El usuario no tiene películas en la lista de favoritos' });
      }
    } catch (error) {
      console.error('Error:', error);
      return res.status(500).json({ error: error.message });
    }
  });

  app.post('/api/comentarios', async (req, res) => {
    try {
        const peliculaID = req.body.peliculaID;
        const userUID = req.body.uid;
        const comentarioText = req.body.comentario;

        const peliculaRef = db.collection('peliculas').doc(peliculaID);
        const peliculaDoc = await peliculaRef.get();

        if (!peliculaDoc.exists) {
            return res.status(404).json({ error: 'Película no encontrada' });
        }

        const userRef = db.collection('users').where('uid', '==', userUID);
        const userSnapshot = await userRef.get();

        if (userSnapshot.empty) {
            return res.status(404).json({ error: 'Usuario no encontrado' });
        }

        const userID = userSnapshot.docs[0].id;

        const comentarioRef = db.collection('comentarios')
            .where('peliculaID', '==', peliculaID)
            .where('userID', '==', userID);

        const comentarioSnapshot = await comentarioRef.get();

        if (comentarioSnapshot.empty) {
            await db.collection('comentarios').doc().create({
                peliculaID: peliculaID,
                userID: userID,
                comentario: comentarioText,
            });

            return res.status(200).json({ message: 'Comentario agregado exitosamente' });
        } else {
            const comentarioID = comentarioSnapshot.docs[0].id;
            await db.collection('comentarios').doc(comentarioID).update({
                comentario: comentarioText,
            });

            return res.status(200).json({ message: 'Comentario actualizado exitosamente' });
        }
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ error: error.message });
    }
});

exports.app = functions.https.onRequest(app);