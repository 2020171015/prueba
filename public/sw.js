const CACHE_STATIC_NAME = 'static-v1';
const CACHE_DYNAMIC_NAME = 'dynamic-v1';
const CACHE_INMUTABLE = 'inmutable-v1';

self.addEventListener('install', (event) => {
    console.log('Instalando Service Worker...');
    event.waitUntil(
        caches.open(CACHE_STATIC_NAME).then((cache) => {
            cache.addAll([
                '/',
                '/login',
                '/register',
                '/profile',
                '/favoritos',
                '/index.html',
                '/js/app.js',
                '/sw.js',
                'static/js/bundle.js',
                'manifest.json',
            ]);
        })
    );
});

self.addEventListener('activate', (event) => {
    console.log('Activando Service Worker...');
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(
                keyList.map((key) => {
                    if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
                        return caches.delete(key);
                    }
                })
            );
        })
    );
});

self.addEventListener('fetch', (event) => {
    if (event.request.url.startsWith('chrome-extension://')) {
        return fetch(event.request);
    }

    event.respondWith(
        fetch(event.request)
            .then((res) => {
                // Si la respuesta es parcial, simplemente devuelve la respuesta sin almacenarla en caché
                if (res.status === 206) {
                    return res;
                }

                // Clona la respuesta antes de almacenarla en la caché
                const clonedResponse = res.clone();

                return caches.open(CACHE_DYNAMIC_NAME).then((cache) => {
                    cache.put(event.request.url, clonedResponse);
                    return res;
                });
            })
            .catch(() => {
                return caches.match(event.request);
            })
    );
});

